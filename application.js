
var scores, score, activePlayer, game;
//début du jeu avec la fonction startgame
startGame();
document.querySelector('.btn-roll').addEventListener('click', () => {
    if(game) {
        //sélection de la valeur dice en aléatoire
        var dice = Math.floor(Math.random() * 6) + 1;
        var diceDOM = document.querySelector('#dice');
        diceDOM.src = 'images/dice-' + dice + '.png';
        if (dice !== 1) {
        //ajout du de la valeur au score courant
        score += dice;
        // son au lancer de dés
        
        document.querySelector('#current-'+ activePlayer ).textContent = score;
        var audio = new Audio('son/throw-dice.wav');
        audio.play();
        } else {
            // message quand le dés tiré et 1 avec message et son de changmement de tour
            document.querySelector('#play-' + activePlayer).textContent = 'Désolé vous passer votre tour!';
            var audio = new Audio('son/change-turns.wav');
            audio.play();
            nextPlayer();
            // réinitialisation du message 
            document.querySelector('#play-' + activePlayer ).textContent = '';
        }
    }    
});

document.querySelector('.btn-hold').addEventListener('click', () =>{
        if(game){
            //ajout du score au score total du joueur actif
            scores[activePlayer] += score;
            document.querySelector('#score-'+activePlayer ).textContent = scores[activePlayer];
            
            if (scores[activePlayer] >= 100) {
                // lorsque le joueur dépasse ou est égal à 100 message et son joueur gagnant
                document.querySelector('#play-' + activePlayer).textContent = 'Féliciatation vous avez gagné!';
                var audio = new Audio('son/win.wav');
                audio.play();
                document.querySelector('.dice').style.display = 'initial';
                document.querySelector('.player-' + activePlayer ).classList.add('win');
                document.querySelector('.player-' + activePlayer ).classList.remove('active');
                game = false;
        }else{
            // changement de joueur
            nextPlayer();
            // réinitialisation du message 
            document.querySelector('#play-' + activePlayer ).textContent = '';
        }
    }
});

document.querySelector('.btn-new').addEventListener('click', startGame); 

function startGame(){
    // réinitilise le jeu
    scores =[ 0, 0];
    activePlayer = 0;
    score = 0;
    game = true;
    
    document.getElementById("score-0").textContent = 0;
    document.getElementById("score-1").textContent = 0;
    document.getElementById("current-0").textContent = 0;
    document.getElementById("current-1").textContent = 0;
    document.getElementById("play-0").textContent = "";
    document.getElementById("play-1").textContent = "";
    document.querySelector('.player-0').classList.remove('win');
    document.querySelector('.player-1').classList.remove('win');
    document.querySelector('.player-0').classList.remove('active');
    document.querySelector('.player-1').classList.remove('active');
    document.querySelector('.player-0').classList.add('active'); 
    document.querySelector('.dice').style.display = 'initial';
}

function nextPlayer() {
    //Changement de joueur
    activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
    score = 0;
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';
    document.querySelector('.player-0').classList.toggle('active');
    document.querySelector('.player-1').classList.toggle('active');
    document.querySelector('.dice').style.display = ''
}




